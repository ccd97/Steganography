package ccd.steganography;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

abstract public class ImageSteganography extends Steganography {

    abstract public BufferedImage encode(File imgFile, File data);
    abstract public File decode(File imgFile, String homePath);

    public File decode(File imgFile, File homePath){
        return decode(imgFile, homePath.getPath());
    }

    public static boolean saveImage(BufferedImage image, String path)
            throws IOException{
        File outputFile = new File(path);
        return ImageIO.write(image, Utils.getFileExtension(path), outputFile);
    }

    public static boolean saveImage(BufferedImage image, File file) throws IOException {
        return ImageIO.write(image, Utils.getFileExtension(file.getPath()), file);
    }

    protected byte[] getImageBytes(BufferedImage image){
        return ((DataBufferByte) image.getRaster().getDataBuffer())
                .getData();

    }
}
