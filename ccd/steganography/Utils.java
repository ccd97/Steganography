package ccd.steganography;

public class Utils {

    static String getFileExtension(String path){
        int idx = path.lastIndexOf(".");
        return path.substring(idx + 1);
    }

    static byte[] intToByteArray(int value, int len){
        byte[] result = new byte[len];

        for(int i=0; i<len; i++){
            result[i] = (byte) (value >> 8*(len-i-1));
        }

        return result;
    }

    public static int byteArrayToInt(byte[] value){
        int result = 0;

        for(int i=0; i<value.length; i++){
            result |= (value[i] & 0xFF) << 8*(value.length-i-1);
        }

        return result;
    }
}
