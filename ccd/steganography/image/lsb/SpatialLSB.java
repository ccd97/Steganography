package ccd.steganography.image.lsb;

import ccd.steganography.Utils;
import ccd.steganography.image.LSB;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

public class SpatialLSB extends LSB {

    private static final int ALGO_ID = 1;
    private static final int metaSize = 2;

    private short separation;

    private SpatialLSB(short separation){
        this.separation = separation;
    }

    public SpatialLSB(int separation){
       this((short) separation);
    }

    private static byte[] getMeta(short separation){
        byte[] meta = new byte[metaSize];
        short metaInt = ALGO_ID<<12;
        separation &= 0x00FF;
        metaInt |= separation;
        meta[0] = (byte)(metaInt & 0xFF);
        meta[1] = (byte)((metaInt >> 8) & 0xFF);
        return meta;
    }

    private static short retrieveMeta(byte[] meta){
        return (short)(((meta[1] & 0x0F) << 8) | (meta[0] & 0xFF));
    }

    private byte[] getByteFromImgBits(byte[] imgBytes, int len, int offset, int sep){
        byte[] result = new byte[len];
        for(int i=0; i<len; i++){
            byte b = 0;
            for(int j=0; j<8*sep; j+=sep){
                b |= (imgBytes[sep*8*i+j+8*offset] & 1) << j/sep;
            }
            result[i] = b;
        }
        return result;
    }

    @Override
    public BufferedImage encode(File imgFile, File data){
        BufferedImage image = null;
        byte[] imgBytes = null;
        byte[] dataBytes = null;
        byte[] metaBytes = null;

        byte dataByte, dataBit, metaByte;
        int i,j,imgPtr;

        try{
            image = ImageIO.read(imgFile);
            imgBytes = getImageBytes(image);
            dataBytes = getDataBytes(data);
            metaBytes = getMeta(separation);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        assert dataBytes != null;
        assert metaBytes != null;

        imgPtr  = 0;

        for(i=0; i<metaBytes.length; i++){
            metaByte = metaBytes[i];
            for(j=0; j<8; j++, imgPtr++) {
                // Replace last bit per pixel
                dataBit = (byte) ((metaByte >> j) & 1);
                imgBytes[imgPtr] = (byte) ((imgBytes[imgPtr] & 0xFE) | dataBit);
            }
        }

        for(i=0; i<dataBytes.length; i++){
            dataByte = dataBytes[i];
            for(j=0; j<8; j++, imgPtr+=separation){
                dataBit = (byte)((dataByte >> j) & 1);
                imgBytes[imgPtr] = (byte)((imgBytes[imgPtr] & 0xFE) | dataBit);
            }
        }

        return image;
    }

    @Override
    public File decode(File imgFile, String homePath){
        File data = null;
        BufferedImage image;
        FileOutputStream fos;
        byte[] imgBytes = null;

        try{
            image = ImageIO.read(imgFile);
            imgBytes = getImageBytes(image);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        byte[] metaBytes = getByteFromImgBits(imgBytes, 2, 0, 1);
        int sep = retrieveMeta(metaBytes);

        byte[] dataLabelSize = getByteFromImgBits(imgBytes, 2, metaSize, sep);
        int sizeOfLabel = Utils.byteArrayToInt(dataLabelSize);

        byte[] dataLabel = getByteFromImgBits(imgBytes, sizeOfLabel, metaSize + 2*sep, sep);

        byte[] dataFileSize = getByteFromImgBits(imgBytes, 4, metaSize + sep*(sizeOfLabel + 2), sep);
        int sizeOfFile = Utils.byteArrayToInt(dataFileSize);

        byte[] dataFile = getByteFromImgBits(imgBytes, sizeOfFile, metaSize + sep*(sizeOfLabel + 6), sep);


        try{
            data = new File(homePath + "\\out_" + new String(dataLabel));
            fos = new FileOutputStream(data);
            fos.write(dataFile);
        } catch(Exception e){
            e.printStackTrace();
        }

        return data;
    }
}
