package ccd.steganography.image.lsb;

import ccd.steganography.Utils;
import ccd.steganography.image.LSB;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

public class LinearLSB extends LSB {

    private static final int ALGO_ID = 0;
    private static final int metaSize = 1;

    private byte[] getByteFromImgBits(byte[] imgBytes, int len, int offset){
        byte[] result = new byte[len];
        for(int i=0; i<len; i++){
            byte b = 0;
            for(int j=0; j<8; j++){
                b |= (imgBytes[8*i+j+8*offset] & 1) << j;
            }
            result[i] = b;
        }
        return result;
    }

    @Override
    public BufferedImage encode(File imgFile, File data){
        BufferedImage image = null;
        byte[] imgBytes = null;
        byte[] dataBytes = null;

        byte dataByte, dataBit;
        int i,j,imgPtr;

        try{
            image = ImageIO.read(imgFile);
            imgBytes = getImageBytes(image);
            dataBytes = getDataBytes(data);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        assert dataBytes != null;

        imgPtr  = 0;
        for(i=0; i<dataBytes.length; i++){
            dataByte = dataBytes[i];
            for(j=0; j<8; j++, imgPtr++){
                // Replace last bit per pixel
                dataBit = (byte)((dataByte >> j) & 1);
                imgBytes[imgPtr] = (byte)((imgBytes[imgPtr] & 0xFE) | dataBit);
            }
        }

        return image;
    }

    @Override
    public File decode(File imgFile, String homePath){
        File data = null;
        BufferedImage image;
        FileOutputStream fos;
        byte[] imgBytes = null;

        try{
            image = ImageIO.read(imgFile);
            imgBytes = getImageBytes(image);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        byte[] dataLabelSize = getByteFromImgBits(imgBytes, 2, 0);
        int sizeOfLabel = Utils.byteArrayToInt(dataLabelSize);

        byte[] dataLabel = getByteFromImgBits(imgBytes, sizeOfLabel, 2);

        byte[] dataFileSize = getByteFromImgBits(imgBytes, 4, 2 + sizeOfLabel);
        int sizeOfFile = Utils.byteArrayToInt(dataFileSize);

        byte[] dataFile = getByteFromImgBits(imgBytes, sizeOfFile, 6 + sizeOfLabel);


        try{
            data = new File(homePath + "\\out_" + new String(dataLabel));
            fos = new FileOutputStream(data);
            fos.write(dataFile);
        } catch(Exception e){
            e.printStackTrace();
        }

        return data;
    }
}
