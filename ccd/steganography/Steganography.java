package ccd.steganography;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class Steganography {

    abstract public Object encode(File inputFile, File data);

    abstract public File decode(File outFile, File outData);

    protected byte[] getDataBytes(File data) throws IOException{
        RandomAccessFile raFile = new RandomAccessFile(data, "r");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int fileLen = Math.toIntExact(raFile.length());

        byte[] dataFile = new byte[fileLen];
        byte[] dataFileSize = Utils.intToByteArray(fileLen, 4);
        byte[] dataLabel = data.getName().getBytes();
        byte[] dataLabelSize = Utils.intToByteArray(data.getName().length(), 2);
        raFile.read(dataFile);

        baos.write(dataLabelSize);
        baos.write(dataLabel);
        baos.write(dataFileSize);
        baos.write(dataFile);

        return baos.toByteArray();
    }

}
